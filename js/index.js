function validate() {
  validatePassword($(".passwordTextField").val());
  validateEmail($(".emailTextField").val());
}

function setUpHTMLFixture() {
  jasmine
    .getFixtures()
    .set(
      ' <p> jasmine jquery form validation </p>  \
                                    <div class="password-strength"></div>    \
                                    <br/>            \
                                    <div class="validate-email"></div>      \
                                    <br/>        \
                                    <input type="text" class="passwordTextField">        \
                                    <br/>        \
                                    <input type="text" class="emailTextField">      \
                                    <br/>        \
                                    <button class="validate" onclick=validate()>Validate</button>              \
                                '
    );
}

function validatePassword(newPassword) {
  var len = newPassword.length,
    passwordIndicator = $(".password-strength");

  if (len == 0) {
    passwordIndicator.text("Please do not leave the password field empty!");
  } else if (len < 6) {
    passwordIndicator.text("Password too short. Enter atleast 6 characters");
  } else {
    passwordIndicator.text("");
  }
}

function validateEmail(newInput) {
  var len = newInput.length;
  numberIndicator = $(".emailTextField");

  if (len == 0) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    numberIndicator.text("Please do not leave email field empty");
  } else if (!emailInput.value.match(mailformat)) {
    numberIndicator.text("Please enter a correct email");
  }
}

describe("Validating password field", function () {
  var passwordLabel;
  beforeEach(function () {
    setUpHTMLFixture();
    passwordLabel = $(".password-strength");
  });

  it("should alert when password is left blank", function () {
    $(".passwordTextField").val("");
    $(".validate").click();
    expect(passwordLabel).not.toBeEmpty();
  });

  it("should alert when password is less than 6 chars", function () {
    $(".passwordTextField").val("abcde");
    $(".validate").click();
    expect(passwordLabel).not.toBeEmpty();
  });

  it("should raise no errors if it is valid", function () {
    $(".passwordTextField").val("validPasswd");
    $(".validate").click();
    expect(passwordLabel).toBeEmpty();
  });
});

describe("Validating Email field", function () {
  var numberLabel;
  beforeEach(function () {
    setUpHTMLFixture();
    numberLabel = $(".validate-email");
  });

  it("should alert when input is blank", function () {
    $(".emailTextField").val("");
    $(".validate").click();
    expect(numberLabel).not.toBeEmpty();
  });

  it("should alert when input is not an email", function () {
    $(".emailTextField").val("someString");
    $(".validate").click();
    expect(numberLabel).not.toBeEmpty();
  });
  it("should alert when email format is not correct", function () {
    $(".emailTextField").val("abc.com");
    $(".validate").click();
    expect(numberLabel).not.toBeEmpty();
  });

  it("should raise no errors if it is valid", function () {
    $(".emailTextField").val("1");
    $(".validate").click();
    expect(numberLabel).toBeEmpty();
  });
});
