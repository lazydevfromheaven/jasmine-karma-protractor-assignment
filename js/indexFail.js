function validate() {
  validatePassword($(".passwordTextField").val());
  validateEmail($(".emailTextField").val());
}

function setUpHTMLFixture() {
  jasmine
    .getFixtures()
    .set(
      ' <p> jasmine jquery form validation </p>  \
                                    <div class="password-strength"></div>    \
                                    <br/>            \
                                    <div class="validate-email"></div>      \
                                    <br/>        \
                                    <input type="text" class="passwordTextField">        \
                                    <br/>        \
                                    <input type="text" class="emailTextField">      \
                                    <br/>        \
                                    <button class="validate" onclick=validate()>Validate</button>              \
                                '
    );
}

function validatePassword(newPassword) {
  var len = newPassword.length,
    passwordIndicator = $(".password-strength");

  if (len == 0) {
    passwordIndicator.text("Please do not leave the password field empty!");
  } else if (len < 6) {
    passwordIndicator.text("Password too short. Enter atleast 6 characters");
  } else {
    passwordIndicator.text("");
  }
}

function validateEmail(newInput) {
  var len = newInput.length;
  numberIndicator = $(".validate-email");
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (len == 0) {
    numberIndicator.text("Please do not leave email field empty");
  } else if (!newInput.value.match(mailformat)) {
    numberIndicator.text("Please enter a correct email");
  }
}
const assert = require("assert");

describe("Login Page", function () {
  it("should let you log in", function () {
    browser.url("/");
    browser.setValue('input[name="email"]', "valid@user.com");
    browser.setValue('input[name="password"]', "hunter2");
    browser.click(".button=Login");

    const pageUrl = browser.getUrl();
    assert.notEqual(pageUrl, "http://127.0.0.1:5500/form.html");
    assert.equal(
      pageUrl,
      "http://127.0.0.1:5500/form.html?email=valid%40user.com&password=hunter2"
    );
  });
});
describe("Validating password field", function () {
  var passwordLabel;
  beforeEach(function () {
    setUpHTMLFixture();
    passwordLabel = $(".password-strength");
  });

  it("should alert when password is left blank", function () {
    $(".passwordTextField").val("");
    $(".validate").click();
    expect(passwordLabel).not.toBeEmpty();
  });

  it("should alert when password is less than 6 chars", function () {
    $(".passwordTextField").val("abcde");
    $(".validate").click();
    expect(passwordLabel).not.toBeEmpty();
  });

  it("should raise no errors if it is valid", function () {
    $(".passwordTextField").val("inf");
    $(".validate").click();
    expect(passwordLabel).toBeEmpty();
  });
});

describe("Validating email field", function () {
  var numberLabel;
  beforeEach(function () {
    setUpHTMLFixture();
    numberLabel = $(".validate-email");
  });

  it("should alert when input is blank", function () {
    $(".emailTextField").val("");
    $(".validate").click();
    expect(numberLabel).not.toBeEmpty();
  });

  it("should alert when input not correct format", function () {
    $(".emailTextField").val("someString");
    $(".validate").click();
    expect(numberLabel).not.toBeEmpty();
  });

  it("should raise no errors if it is valid", function () {
    $(".emailTextField").val("valid@user.com");
    $(".validate").click();
    expect(numberLabel).toBeEmpty();
  });
});
